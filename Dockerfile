# From defines the base image for out new image
FROM node:16
WORKDIR /app
COPY public ./public
COPY src ./src
COPY package.json ./
RUN npm i 
RUN ls -al
EXPOSE 3000
CMD ["npm","start"]